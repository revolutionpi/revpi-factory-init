# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later

__author__ = "KUNBUS GmbH"
__copyright__ = "Copyright (C) 2023 KUNBUS GmbH"
__license__ = "GPLv2"
__version__ = "0.0.1"
