# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import sys
from typing import Literal

logger = logging.getLogger()
LoggingLevel = Literal[
    "CRITICAL",
    "FATAL",
    "ERROR",
    "WARN",
    "WARNING",
    "INFO",
    "DEBUG",
    "NOTSET",
]


def setup_logger() -> None:
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(name)s: %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def logger_set_level(level: LoggingLevel) -> None:
    logger.setLevel(logging._nameToLevel[level])
