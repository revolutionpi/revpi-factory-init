# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later

from enum import Enum, auto


class PwSrc(Enum):
    """Enumeration for specifying the source of passwords for a RevPi device."""

    TPM = auto()
    CRYPTO_CHIP = auto()


class RevPiDevice:
    """
    Defines a generic RevPi device with common attributes.

    Parameters:
    -   device_type (str): The type of RevPi device.
    -   serial_number (str): The serial number of the device.
    -   mac_address (str): The primary MAC address of the device.
    -   eth_count (int): The count of Ethernet ports on the device.
    -   pw_src (PwSrc): The source of passwords for the device. PwSrc.TPM or PwSrc.CRYPTO_CHIP
    -   ks8851 (tuple[str, int], optional): A tuple containing the interface name and MAC address offset of the KS8851 Ethernet chip.
    """

    def __init__(
        self,
        device_type: str,
        serial_number: str,
        mac_address: str,
        eth_count: int,
        pw_src: PwSrc,
        ks8851: tuple[str, int] = None,  # (interface name, mac address offset)
    ) -> None:
        self.device_type = device_type
        self.serial_number = serial_number
        self.mac_address = mac_address
        self.eth_count = eth_count
        self.ks8851_eeprom = ks8851
        self.pw_src = pw_src


class RevPiDeviceFactory:
    """Factory class for creating instances of RevPI device types."""

    @staticmethod
    def get_device(
        device_type: str, serial_number: str, mac_address: str
    ) -> RevPiDevice | None:
        """Creates and returns an instance of a specified RevPi device type."""

        tmp_device = None

        match device_type:
            case "core":
                tmp_device = RevPiDevice(
                    "core",
                    serial_number,
                    mac_address,
                    eth_count=1,
                    pw_src=PwSrc.CRYPTO_CHIP,
                )

            case "compact":
                tmp_device = RevPiDevice(
                    "compact",
                    serial_number,
                    mac_address,
                    eth_count=2,
                    pw_src=PwSrc.CRYPTO_CHIP,
                    ks8851=("eth1", 1),
                )

            case "connect":
                tmp_device = RevPiDevice(
                    "connect",
                    serial_number,
                    mac_address,
                    eth_count=2,
                    pw_src=PwSrc.CRYPTO_CHIP,
                )

            case "connect-se":
                tmp_device = RevPiDevice(
                    "connect-se",
                    serial_number,
                    mac_address,
                    eth_count=2,
                    pw_src=PwSrc.CRYPTO_CHIP,
                )

            case "flat":
                tmp_device = RevPiDevice(
                    "flat", serial_number, mac_address, eth_count=2, pw_src=PwSrc.TPM
                )

        return tmp_device

    @staticmethod
    def get_device_types() -> list[str]:
        """Returns a list of available RevPi Pi device types."""

        return ["core", "compact", "connect", "connect-se", "flat"]
